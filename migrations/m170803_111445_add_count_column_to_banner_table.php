<?php

use yii\db\Migration;

/**
 * Handles adding count to table `banner`.
 */
class m170803_111445_add_count_column_to_banner_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn('banner', 'count', $this->integer(1)->defaultValue(1));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn('banner', 'count');
    }
}

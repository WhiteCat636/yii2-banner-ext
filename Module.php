<?php

namespace common\modules\banner;


use Yii;
use yii\base\NotSupportedException;

class Module extends \yii\base\Module
{
	const APP_TYPE_FRONTEND = 'frontend';
	const APP_TYPE_BACKEND = 'backend';

	/**
	 * @var string controllers path template.
	 */
	private $controllersPathTemplate = 'dkit\banner\%s\controllers';

	/**
	 * @var string views alias path template.
	 */
	private $viewsAliasTemplate = '@dkit/banner/%s/views';

	/**
	 * @var string application type.
	 * @example self::APP_TYPE_FRONTEND or self::APP_TYPE_BACKEND
	 */
	public $appType = self::APP_TYPE_FRONTEND;

	/**
	 * @var array access rules for frontend part of module.
	 * By default all actions are allowed.
	 */
	public $frontendAccessRules = [['allow' => true]];

	/**
	 * @var array access rules for backend part of module.
	 * By default all actions are forbidden.
	 */
	public $backendAccessRules = [];

	/**
	 * @var array available banner sizes binding to type of.
	 */
	public $bannerSize = [
		'main'          => ['width' => 240, 'height' => 240],
		'sponsors'      => ['width' => 125, 'height' => 50],
		'events'        => ['width' => 200, 'height' => 200],
		'journal'       => ['width' => 200, 'height' => 285],
		'horizontal 1'  => ['width' => 560, 'height' => 80],
		'horizontal 2'  => ['width' => 850, 'height' => 120],
	];

	/**
	 * @var boolean|false|string whether to use own method to get current app language.
	 * To use own method pass a namespace of class which has static method [[Class::getAppLanguageCode()]]
	 * @example 'app\frontend\components\Language'
	 */
	public $ownLanguageGetter = false;

	/**
	 * @var array list of available languages for posts.
	 */
	public $languages = [
		'uk' => 'Українська',
		'ru' => 'Русский',
		'en' => 'English'
	];

	/**
	 * @var array URL and real PATH to folder, where images should be stored.
	 * You can set two aliases in [[common/config/bootstrap.php]], like done below:
	 * ```php
	 * Yii::setAlias('storageUrl', 'http://storage.example.com');
	 * Yii::setAlias('storagePath', dirname(dirname(dirname(__DIR__))) . '/storage');
	 * ```
	 */
	public $imgPaths = [
		'url' => '@storageUrl/uploads/banners',
		'path' => '@storagePath/uploads/banners',
	];


	/**
	 * Returns a list of behaviors that this component should behave as.
	 * @return array the behavior configurations.
	 */
	public function behaviors()
	{
		return [
			'access' => [
				'class' => 'yii\filters\AccessControl',
				'rules' => $this->isBackend() ? $this->backendAccessRules : $this->frontendAccessRules,
			]
		];
	}

	/**
	 * Initializes the module.
	 * If you override this method, please make sure you call the parent implementation.
	 * @return void
	 */
	public function init()
	{
		parent::init();
//		$this->setControllerNamespace($this->appType);
//		$this->setViewPath($this->appType);
	}

	/**
	 * ControllerNamespace setter.
	 * @param string $appType application type.
	 * @return void
	 */
	public function setControllerNamespace($appType)
	{
		$this->controllerNamespace = sprintf($this->controllersPathTemplate, $appType);
	}

	/**
	 * Override method for parent viewPath setter.
	 * @param string $appType application type.
	 * @uses [[\yii\base\Module::setViewPath()]]
	 * @return void
	 */
	public function setViewPath($appType)
	{
		parent::setViewPath(sprintf($this->viewsAliasTemplate, $appType));
	}

	/**
	 * Get current app language code.
	 * @uses [[$this->ownLanguageGetter]] to define in case you want define own language getter method.
	 * @return string
	 */
	public function getAppLanguageCode()
	{
		if ($this->ownLanguageGetter && is_string($this->ownLanguageGetter)) {
			$class = $this->ownLanguageGetter;
			return $class::getAppLanguageCode();
		} else {
			if (preg_match('/-/', Yii::$app->language)) {
				return explode('-', Yii::$app->language)[0];
			} else {
				return Yii::$app->language;
			}
		}
	}

	/**
	 * Check if module used in backend scenario.
	 * Means not yii\base\Model::$scenario
	 * @todo add support for Yii 2 Basic Project Template.
	 * @throws NotSupportedException
	 * @return boolean
	 */
	public function isBackend()
	{
		if ($this->appType === self::APP_TYPE_BACKEND) {
			return true;
		} elseif ($this->appType === self::APP_TYPE_FRONTEND) {
			return false;
		} else {
			throw new NotSupportedException('Used application type is not supported');
		}
	}

}

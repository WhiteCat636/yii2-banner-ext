Banner Ext
==========
extension yii2-banner

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist dk-it/yii2-banner-ext "*"
```

or add

```
"dk-it/yii2-banner-ext": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \dkit\banner_ext\AutoloadExample::widget(); ?>```
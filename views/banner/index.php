<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\Banner */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Banners');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Banner'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel'  => $searchModel,
        'columns'      => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            //'type',
            'place',
            'title',
            'pages_url:ntext',
            //'languages_enabled',
            // 'img',
            // 'url:url',
            'date_start',
            'date_end',
            // 'clicks',

            [
                'class'    => 'yii\grid\ActionColumn',
                'buttons'  => [
                    'view'      => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url,
                            ['class' => 'btn btn-success btn-sm']);
                    },
                    'update'    => function ($url, $model, $key) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url,
                            ['class' => 'btn btn-warning btn-sm']);
                    },
                    'delete'    => function ($url, $model, $key) {
                        return Html::a(
                            '<span class="glyphicon glyphicon-trash"></span>',
                            $url, [
                                'class'        => 'btn btn-danger btn-sm',
                                'data-confirm' => \Yii::t('yii', 'Are you sure you want to delete this item?'),
                                'data-method'  => 'post',
                            ]
                        );
                    },
                ],
                'template' => '<div style="width:75px;margin-bottom: 5px;" class="clearfix">
                                <div class="pull-left">{view_site}</div> <div class="pull-right">{view}</div>
                            </div>
                            <div style="width:75px" class="clearfix">
                                <div class="pull-left">{update}</div> <div class="pull-right">{delete}</div>
                            </div>',
            ],
        ],
    ]); ?>
</div>

<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 21/12/16
 * Time: 15:03
 */

namespace common\modules\banner\widgets;

//use dkit\banner\common\components\ImageComponent;


use common\components\ImageComponent;
use common\modules\banner\models\Banner;
use yii\base\Widget;

class BannerWidget extends Widget
{
    public $item;
    public $type = 'main';
    public $limit = 1;


    /**
     * @var string message that is shown when there is no items.
     */
    public $noItemsMessage = 'Nothing found...';
    /**
     * @var array list of available tokens to be used while rendering each item.
     * You can specify your own tokens, to do so, you should also specify
     * corresponding token handler in an array of [[$tokensHandlers]]
     */
    public $tokens = [
        '{url}',
        '{img}',
    ];
    /**
     * @var string widget wrapper markup.
     * The following tokens will be replaced when [[renderWidget()]] is called:
     * `{items}`    fetched items,
     * `{no_items_message}` text that is shown when there are no items.
     */
    public $wrapperTemplate = '{items}{no_items_message}';
    public $itemTemplate = '
        <a href="{url}">
            <img src="{img}" class="img-responsive" alt="">
        </a>
    ';

    private $_items;

    public function run()
    {
        $items = $this->renderItemsToken();
        $output = strtr($this->wrapperTemplate, [
            '{items}' => $items,
            '{no_items_message}' => !$items ? $this->noItemsMessage : false,
        ]);
        return $output;
    }

    public function getItems ()
    {
        return $this->_items = Banner::find()
//            ->localized()
            ->showPeriod()
            ->ordered()
            ->asArray()
            ->limit($this->limit)
            ->andWhere(['type' => $this->type])
            ->all();
    }

    /**
     * Render {items} token from [[$wrapperTemplate]]
     * @return string
     */
    public function renderItemsToken()
    {
        $output = '';
        $items = $this->items;

        foreach ($items as $model) {
            // Loop with check for first and last element
           $output .= strtr($this->itemTemplate, $this->callTokenHandlers($model));

        }
        return $output;
    }

    /**
     * Calls collected token handlers.
     * @param array $model Banner model presented as an array
     * @return mixed
     */
    public function callTokenHandlers($model)
    {
        if (!empty($this->tokens)) {
            $adjustedHandlers = [];
            foreach ($this->tokens as $token) {
                $adjustedHandlers[$token] = call_user_func($this->getTokenHandler($token), $model);
            }
            return $adjustedHandlers;
        } else {
            throw new InvalidParamException('List of tokens not specified');
        }
    }

    /**
     * Get predefined (default) token handler.
     * @param string $token name.
     * @return \Closure
     */
    public function getTokenHandler($token)
    {
        $defaultTokenHandlers = [
            '{url}' => function ($model) {return $model['url'];},
            '{img}' => function ($model) {return ImageComponent::thumbnail($model['img'], $this->type);},

        ];
        return $defaultTokenHandlers[$token];
    }

}
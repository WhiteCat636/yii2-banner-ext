<?php

namespace dkit\banner_ext\models;

use frontend\helpers\LangHelper;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[Banner]].
 *
 * @see Banner
 */
class BannerQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return Banner[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Banner|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    /**
     * Set [[andWhere]] query so that sections are fetched only for current language.
     * @return $this
     */
    public function localized()
    {
        return $this->andWhere(['like', 'banner.languages_enabled', LangHelper::getLang()]);
    }

    public function showPeriod()
    {
        return $this->andWhere(['<=', 'banner.date_start', date('Y-m-d H:i:s')])
            ->andWhere(['>=', 'banner.date_end', date('Y-m-d H:i:s')]);
    }

    public function byUrl($bool)
    {
        $url = Yii::$app->request->absoluteUrl;
        $url = $this->cleanUrl($url);
        

        if ($bool) {
            return $this->andWhere(['pages_url'=>$url]);
        }

        return $this->andWhere(['pages_url' => '']);
    }
    /**
     * Set [[orderBy]] query so that sections are ordered by `nav_sort` field.
     * @return $this
     */
    public function ordered()
    {
        return $this->orderBy(['banner.order' => SORT_ASC]);
    }
    public function cleanUrl($url){
        //clear page number
        if (strpos($url, '?page=')) {
            $explodeUrl = explode('?', $url);
            $url = $explodeUrl[0];
        };
        //clear language
        if(!empty(Yii::$app->urlManager->languages)){
            $domains = explode('/', ltrim($url, '/'));
            $lang = Yii::$app->params['defaultLanguage'];
            for ($i=0; $i<count($domains); $i++) {
                if ( in_array($domains[$i], array_keys(Yii::$app->urlManager->languages))){
                        $lang = $domains[$i];
                        continue;
                }
            }
            Yii::$app->language = $lang;
            $url = str_replace('/'.$lang, '', $url);// просто убрать его из ссылки, если он там есть 
        }
        return $url;
    }
}

<?php

namespace dkit\banner_ext\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Banner represents the model behind the search form about `common\models\Banner`.
 */
class BannerSearch extends Banner
{
    /**
     * @inheritdoc
     */
    public $date_start = '';
    public $date_end = '';
    public function rules()
    {
        return [
            [['id', 'clicks'], 'integer'],
            [['type', 'place', 'pages_url', 'languages_enabled', 'img', 'url', 'date_start', 'date_end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Banner::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date_start' => $this->date_start,
            'date_end' => $this->date_end,
            'clicks' => $this->clicks,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type])
            ->andFilterWhere(['like', 'place', $this->place])
            ->andFilterWhere(['like', 'pages_url', $this->pages_url])
            ->andFilterWhere(['like', 'languages_enabled', $this->languages_enabled])
            ->andFilterWhere(['like', 'img', $this->img])
            ->andFilterWhere(['like', 'url', $this->url]);

        return $dataProvider;
    }
}

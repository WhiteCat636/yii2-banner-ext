<?php

namespace dkit\banner_ext\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property integer $id
 * @property string $type
 * @property string $place
 * @property string $pages_url
 * @property string $languages_enabled
 * @property string $img
 * @property integer $half
 * @property string $url
 * @property string $date_start
 * @property string $date_end
 * @property integer $clicks
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $date_start = '2016-01-01 12:30';
    public $date_end = '2018-01-01 12:30';
    public $count  = 1;
    public static function tableName()
    {
        return 'banner';
    }
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->pages_url = $this->find()->cleanUrl($this->pages_url);
            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['place', 'title', 'img', 'url', 'date_start', 'date_end'], 'required'],
            [['pages_url'], 'string'],
            [['date_start', 'date_end'], 'safe'],
            [['clicks'], 'integer'],
            [['count'], 'integer'],
            [['type', 'place', 'title'], 'string', 'max' => 55],
            [['languages_enabled'], 'string', 'max' => 11],
            [['img', 'url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'title' => Yii::t('app', 'Title'),
            'place' => Yii::t('app', 'Place'),
            'pages_url' => Yii::t('app', 'Pages Url'),
            'languages_enabled' => Yii::t('app', 'Languages Enabled'),
            'img' => Yii::t('app', 'Img'),
            'count' => Yii::t('app', 'Count'),
            'url' => Yii::t('app', 'Url'),
            'date_start' => Yii::t('app', 'Date Start'),
            'date_end' => Yii::t('app', 'Date End'),
            'clicks' => Yii::t('app', 'Clicks'),
        ];
    }

    /**
     * @inheritdoc
     * @return BannerQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new BannerQuery(get_called_class());
    }
}
